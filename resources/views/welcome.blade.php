<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <style>
            .container {
              display: flex;
              justify-content: center;
              align-items: center;
              height: 100vh;
            }
            
        button {
            padding: 12px 24px;
            border: none;
            border-radius: 4px;
            background-color: rgb(42, 127, 255);
            color: white;
            font-size: 16px;
            cursor: pointer;
        }
        </style>
    </head>
    <body>
        <div class="container">
          <button>API AUTH LARAVEL 10</button>
        </div>
      </body>
</html>